package org.gredel.sorting.Sort;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class SortingTestTenArgs {
    Sorting sorting = new Sorting();
    private Integer[] array;
    private Integer[] expected;

    public SortingTestTenArgs(Integer[] array, Integer[] expected) {
        this.array = array;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Integer[][][] {
                {{1,4,2,3,6,7,8,5,9,0}, {0,1,2,3,4,5,6,7,8,9}},
                {{1,2,3,4,5,6,7,8,9,10}, {1,2,3,4,5,6,7,8,9,10}},
                {{7,8,9,10,1,2,3,4,5,6}, {1,2,3,4,5,6,7,8,9,10}},
                {{4,2,1,5,3,2,6,7,8,9}, {1,2,2,3,4,5,6,7,8,9}}
        });
    }

    @Test
    public void testTenArg(){
        sorting.sortAsc(array);
        assertTrue(Arrays.equals(array, expected));
    }

}
