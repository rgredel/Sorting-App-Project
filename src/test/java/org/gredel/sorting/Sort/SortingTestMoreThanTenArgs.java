package org.gredel.sorting.Sort;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class SortingTestMoreThanTenArgs {
    Sorting sorting = new Sorting();
    private Integer[] array;


    public SortingTestMoreThanTenArgs(Integer[] array) {
        this.array = array;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Integer[][][] {
                {{1,4,2,3,6,7,8,5,9,0,2}},
                {{1,2,3,4,5,6,7,8,9,10,2}},
                {{7,8,9,10,1,2,3,4,5,6,2}},
                {{4,2,1,5,3,2,6,7,8,9,2,2}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArg(){
        sorting.sortAsc(array);
    }

}
