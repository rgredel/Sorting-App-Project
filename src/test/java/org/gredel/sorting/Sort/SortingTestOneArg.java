package org.gredel.sorting.Sort;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class SortingTestOneArg {
    Sorting sorting = new Sorting();
    private Integer[] array;
    private Integer[] expected;

    public SortingTestOneArg(Integer[] array, Integer[] expected) {
        this.array = array;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Integer[][][] {
                {{1}, {1}},
                {{13}, {13}},
                {{4}, {4}},
                {{2}, {2}}
        });
    }

    @Test
    public void testOneArg(){
        sorting.sortAsc(array);
        assertTrue(Arrays.equals(array, expected));
    }

}
