package org.gredel.sorting.Sort;

import junit.framework.TestCase;

import org.junit.Test;


public class SortingTest extends TestCase {
    Sorting sorting = new Sorting();

    @Test
    public void testZeroArg(){
        Integer array[] = new Integer[0];
        sorting.sortAsc(array);
    }

    @Test
    public void testNullArray(){
        Integer [] array  = null;

        try {
            sorting.sortAsc(array);
        }catch (IllegalArgumentException e){
            assertEquals("Array can't be null", e.getMessage());
        }
    }

}