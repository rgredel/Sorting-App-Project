package org.gredel.sorting.UI;

import org.gredel.sorting.Sort.Sorting;

import java.util.Scanner;

public class Menu {

    public void view() {
        while (true) {
            Sorting sorting = new Sorting();
            Scanner sc = new Scanner(System.in);

            System.out.println("Provide array size: ");
            int arrayLength = sc.nextInt();

            Integer array[] = new Integer[arrayLength];

            for (int i = 0; i < arrayLength; i++) {
                System.out.println("i[" + i + "]: ");
                array[i] = sc.nextInt();
            }
            try {
                sorting.sortAsc(array);
            }catch (IllegalArgumentException e){
                System.out.println(e.getMessage());
                continue;
            }

        }
    }
}
