package org.gredel.sorting.Sort;

public class Sorting {

    public void sortAsc(Integer[] array) throws IllegalArgumentException {

        if (array == null) throw new IllegalArgumentException("Array can't be null");
        else if (array.length > 10) throw new IllegalArgumentException("Array size can't be greater than 10");
        int i, j;
        for (i = 0; i < array.length - 1; i++) {
            for (j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j + 1];
                    array[j + 1] = array[j];
                    array[j] = temp;
                }
            }
        }

        printArray(array);
    }

    private void printArray(Integer[] array) {
        System.out.print("[ ");
        for (int element : array) {
            System.out.print(element + " ");
        }
        System.out.println("]");
    }
}
